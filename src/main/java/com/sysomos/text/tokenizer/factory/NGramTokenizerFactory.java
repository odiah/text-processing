package com.sysomos.text.tokenizer.factory;

import java.io.InputStream;

import com.sysomos.text.tokenizer.NGramTokenizer;
import com.sysomos.text.tokenizer.TokenPreProcess;
import com.sysomos.text.tokenizer.Tokenizer;

/**
 * @author sonali
 */
public class NGramTokenizerFactory implements TokenizerFactory {
	private TokenPreProcess preProcess;
	private Integer minN = 1;
	private Integer maxN = 1;
	private TokenizerFactory tokenizerFactory;

	public NGramTokenizerFactory(TokenizerFactory tokenizerFactory,
			Integer minN, Integer maxN) {
		this.tokenizerFactory = tokenizerFactory;
		this.minN = minN;
		this.maxN = maxN;
	}

	@Override
	public Tokenizer create(String toTokenize) {
		if (toTokenize == null || toTokenize.isEmpty()) {
			throw new IllegalArgumentException(
					"Unable to proceed; no sentence to tokenize");
		}

		Tokenizer t1 = tokenizerFactory.create(toTokenize);
		t1.setTokenPreProcessor(preProcess);
		Tokenizer ret = new NGramTokenizer(t1, minN, maxN);
		return ret;
	}

	@Override
	public Tokenizer create(InputStream toTokenize) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setTokenPreProcessor(TokenPreProcess preProcessor) {
		this.preProcess = preProcessor;
	}
}
