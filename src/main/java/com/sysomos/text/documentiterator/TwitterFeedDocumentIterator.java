package com.sysomos.text.documentiterator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sysomos.text.sentenceiterator.TweetItem;

public class TwitterFeedDocumentIterator extends FileDocumentIterator {

	private static final long serialVersionUID = 3564257304610147513L;

	private static final Logger log = LoggerFactory
			.getLogger(TwitterFeedDocumentIterator.class);

	public TwitterFeedDocumentIterator(String path) {
		super(path);
	}

	public TwitterFeedDocumentIterator(File file) {
		super(file);
	}

	@Override
	public InputStream nextDocument() {
		try {
			if (lineIterator != null && !lineIterator.hasNext()
					&& iter.hasNext()) {
				File next = iter.next();
				lineIterator.close();
				lineIterator = FileUtils.lineIterator(next);
				while (!lineIterator.hasNext()) {
					lineIterator.close();
					lineIterator = FileUtils.lineIterator(next);
				}
			}
			if (lineIterator.hasNext()) {
				return new BufferedInputStream(
						IOUtils.toInputStream(parse(lineIterator.nextLine())));
			}
		} catch (Exception e) {
			log.warn("Warning: Couldn't read input stream. [ "
					+ e.getLocalizedMessage() + " ]", e);
			return null;
		}

		return null;
	}

	private String parse(String json) {
		try {
			TweetItem tweetItem = new ObjectMapper().readValue(json,
					TweetItem.class);
			return tweetItem == null ? null : tweetItem.text;
		} catch (JsonParseException e) {
			log.error(e.getLocalizedMessage(), e);
		} catch (JsonMappingException e) {
			log.error(e.getLocalizedMessage(), e);
		} catch (IOException e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return null;
	}
}
