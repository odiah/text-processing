package com.sysomos.text.sentenceiterator;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HadoopTwitterFeedIterator extends BaseSentenceIterator {

	private static final int HOUR = 1 * 60 * 60 * 1000;
	private static final String HDFS_URL = "hdfs://labanlyhdfscluster/twitter/TTFHRawPost/";
	private static final String CORE_SITE = "/etc/hadoop/conf/core-site.xml";
	private static final String HDFS_SITE = "/etc/hadoop/conf/hdfs-site.xml";
	private static final Logger LOG = LoggerFactory
			.getLogger(HadoopTwitterFeedIterator.class);

	protected volatile RemoteIterator<LocatedFileStatus> hdfsFileIterator;
	protected volatile Queue<String> cache;
	protected volatile LocatedFileStatus currentLocatedFile;
	protected volatile LineIterator currLineIterator;

	private int window;
	private long endDateMillis;
	private long startDateMillis;
	private DateTime currentDate;
	private volatile FileSystem fs;
	private volatile String hdfsUrl;

	public HadoopTwitterFeedIterator(int window) {
		this(HDFS_URL, window);
	}

	public HadoopTwitterFeedIterator(String url, int window) {
		this.window = window;
		this.hdfsUrl = url;
		currentDate = new DateTime().minusDays(1);
		endDateMillis = currentDate.getMillis(); // 1437422400000L;//
		startDateMillis = currentDate.minusDays(this.window).getMillis(); // 1437422400000L;
		cache = new java.util.concurrent.ConcurrentLinkedDeque<String>();
		initialize();
	}

	private void initialize() {
		if (startDateMillis > endDateMillis)
			return;
		System.out.println(
				"Start: " + startDateMillis + " End: " + endDateMillis);
		DateTime date = new DateTime(startDateMillis);
		String suffix = suffix(date.getYear(), date.getMonthOfYear(),
				date.getDayOfMonth(), date.getHourOfDay());

		Configuration configuration = new Configuration();
		configuration.addResource(new Path(CORE_SITE));
		configuration.addResource(new Path(HDFS_SITE));

		configuration.set("fs.hdfs.impl",
				org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		configuration.set("fs.file.impl",
				org.apache.hadoop.fs.LocalFileSystem.class.getName());

		try {
			fs = FileSystem.get(configuration);
			hdfsFileIterator = fs.listFiles(new Path(hdfsUrl + suffix), true);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String nextSentence() {
		if (!hasNext()) {
			return null;
		}
		String ret = null;
		if (!cache.isEmpty()) {
			ret = cache.poll();
			if (preProcessor != null)
				ret = preProcessor.preProcess(ret);
			return ret;
		} else {

			if (currLineIterator == null || !currLineIterator.hasNext())
				nextLineIter();

			for (int i = 0; i < 100000; i++) {
				if (currLineIterator != null && currLineIterator.hasNext()) {
					String line = currLineIterator.nextLine();
					if (line != null)
						cache.add(line);
					else
						break;
				} else
					break;
			}
			if (!cache.isEmpty()) {
				ret = cache.poll();
				if (preProcessor != null)
					ret = preProcessor.preProcess(ret);
				return ret;
			}
		}

		if (!cache.isEmpty())
			ret = cache.poll();
		return ret;

	}

	private void nextLineIter() {
		do {
			if (hasNext(hdfsFileIterator)) {
				try {
					LocatedFileStatus next = hdfsFileIterator.next();
					Path path = next.getPath();
					if (path == null) {
						currentLocatedFile = null;
						return;
					}				
					InputStream inputStream = fs.open(path);
					currentLocatedFile = next;
					if (currLineIterator != null)
						currLineIterator.close();
					currLineIterator = IOUtils
							.lineIterator(
									new BufferedInputStream(
											new GZIPInputStream(inputStream)),
									"UTF-8");
					LOG.info(path.toString());
					return;
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			} else {
				startDateMillis += HOUR;
				initialize();
				if (!hasNext(hdfsFileIterator))
					return;
			}
		} while (hasNext(hdfsFileIterator));
	}

	@Override
	public boolean hasNext() {
		return currLineIterator != null && currLineIterator.hasNext()
				|| hasNext(hdfsFileIterator) || !cache.isEmpty()
				|| startDateMillis < endDateMillis;
	}

	private boolean hasNext(RemoteIterator<LocatedFileStatus> iter) {
		try {
			return hdfsFileIterator != null && hdfsFileIterator.hasNext();
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	public void reset() {
		startDateMillis = currentDate.getMillis(); // currentDate.minusDays(window).getMillis();
		initialize();
	}

	private String suffix(int year, int month, int day, int hour) {
		String monthStr = month < 10 ? "0" + month : String.valueOf(month);
		String dayStr = day < 10 ? "0" + day : String.valueOf(day);
		String hourStr = hour < 10 ? "0" + hour : String.valueOf(hour);
		StringBuilder builder = new StringBuilder();
		builder.append(year).append("/").append(monthStr).append("/")
				.append(dayStr).append("/").append(hourStr);
		return builder.toString();
	}

}
