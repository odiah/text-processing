package com.sysomos.text.sentenceiterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Deprecated
public abstract class BaseTweetItemIterator implements Iterator<TweetItem> {

	private TweetItem tweetItem = null;
	/** The current line. */
	private String cachedLine;
	/** A flag indicating if the iterator has been fully read. */
	private boolean finished = false;
	/** The reader that is being read. */
	private final BufferedReader bufferedReader;

	private static final Logger LOG = LoggerFactory
			.getLogger(BaseTweetItemIterator.class);

	public BaseTweetItemIterator(String json) throws IOException {
		this(IOUtils.toInputStream(json, "UTF-8"));
	}

	public BaseTweetItemIterator(final InputStream stream) {
		bufferedReader = new BufferedReader(new InputStreamReader(stream));
	}

	@Override
	public boolean hasNext() {
		if (cachedLine != null) {
			return true;
		} else if (finished) {
			return false;
		} else {
			try {
				while (true) {
					String line = bufferedReader.readLine();
					if (line == null) {
						finished = true;
						return false;
					} else if (isValidLine(line)) {
						cachedLine = line;
						return true;
					}
				}
			} catch (IOException ioe) {
				close();
				throw new IllegalStateException(ioe);
			}
		}
	}

	/**
	 * Overridable method to validate each line that is returned. This
	 * implementation always returns true.
	 * 
	 * @param line
	 *            the line that is to be validated
	 * @return true if valid, false to remove from the iterator
	 */
	protected boolean isValidLine(String line) {
		return true;
	}

	@Override
	public TweetItem next() {
		if (!hasNext()) {
			throw new NoSuchElementException("No more tweets");
		}
		String currentLine = cachedLine;
		cachedLine = null;
		try {
			tweetItem = new ObjectMapper().readValue(currentLine,
					TweetItem.class);
		} catch (JsonParseException e) {
			LOG.error(e.getLocalizedMessage(), e);
		} catch (JsonMappingException e) {
			LOG.error(e.getLocalizedMessage(), e);
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		return tweetItem;
	}

	/**
	 * Returns the text of tweet
	 */
	public String nextSentence() {
		if (!hasNext())
			return null;
		TweetItem tweetItem = next();
		return tweetItem == null ? null : tweetItem.text;
	}

	/**
	 * Closes the underlying <code>Reader</code> quietly. This method is useful
	 * if you only want to process the first few lines of a larger file. If you
	 * do not close the iterator then the <code>Reader</code> remains open. This
	 * method can safely be called multiple times.
	 */
	public void close() {
		finished = true;
		IOUtils.closeQuietly(bufferedReader);
		cachedLine = null;
	}
}
