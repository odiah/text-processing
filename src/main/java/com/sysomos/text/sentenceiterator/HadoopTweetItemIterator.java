package com.sysomos.text.sentenceiterator;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sysomos.text.filter.BaseTweetItemFilter;

public class HadoopTweetItemIterator extends HadoopTwitterFeedIterator {

	private static final Logger LOG = LoggerFactory
			.getLogger(HadoopTweetItemIterator.class);
	private BaseTweetItemFilter filter;

	public HadoopTweetItemIterator(int window) {
		super(window);
	}

	public HadoopTweetItemIterator(String url, int window) {
		super(url, window);
	}

	public HadoopTweetItemIterator(String url, int window,
			BaseTweetItemFilter filter) {
		super(window);
		this.filter = filter;
	}

	public TweetItem nextTweet() {
		String sentence = super.nextSentence();
		while (StringUtils.isEmpty(sentence) && hasNext()) {
			sentence = super.nextSentence();
		}
		try {
			if (StringUtils.isEmpty(sentence))
				return null;
			TweetItem tweetItem = new ObjectMapper().readValue(sentence,
					TweetItem.class);
			if (filter == null)
				return tweetItem;
			while (hasNext()) {
				if (!filter.exclude(tweetItem))
					return tweetItem;
				sentence = super.nextSentence();
				if (StringUtils.isEmpty(sentence))
					continue;
				tweetItem = new ObjectMapper().readValue(sentence,
						TweetItem.class);
			}
			return filter.exclude(tweetItem) ? null : tweetItem;
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			return null;
		}
	}

	public String nextSentence() {
		if (!hasNext())
			return null;
		TweetItem tweetItem = nextTweet();
		while (tweetItem == null && hasNext()) {
			tweetItem = nextTweet();
		}
		return tweetItem == null ? null : tweetItem.text;
	}
}
