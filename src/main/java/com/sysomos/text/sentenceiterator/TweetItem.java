package com.sysomos.text.sentenceiterator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetItem {
	@JsonProperty("created_at")
	public String createdAt;
	@JsonProperty("id")
	public long id;
	@JsonProperty("id_str")
	public String idStr;
	@JsonProperty("text")
	public String text;
	@JsonProperty("user")
	public User user;
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class User {
		@JsonProperty("id")
		public long id;
	}
}
