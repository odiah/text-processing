package com.sysomos.text.filter.rule;

import java.text.SimpleDateFormat;

import org.joda.time.DateTime;

import com.sysomos.text.sentenceiterator.TweetItem;

public class IgnoreIfCreatedAtNotInRule extends TweetItemRule {

	private final long startDateMillis;
	private final long endDateMillis;

	public IgnoreIfCreatedAtNotInRule(long startDateMillis,
			long endDateMillis) {
		this.startDateMillis = startDateMillis;
		this.endDateMillis = endDateMillis;
	}

	public IgnoreIfCreatedAtNotInRule(int timeWindow) {
		endDateMillis = new DateTime().getMillis();
		startDateMillis = new DateTime().minusDays(timeWindow).getMillis();
	}

	@Override
	public boolean fires(TweetItem item) {
		SimpleDateFormat df = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss Z yyyy");
		try {
			long createdAt = df.parse(item.createdAt).getTime();
			if (startDateMillis > createdAt || endDateMillis < createdAt)
				return true;
		} catch (java.text.ParseException e) {
			getLogger().warn(e.getLocalizedMessage());
			return false;
		}
		return false;
	}

}
