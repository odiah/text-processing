package com.sysomos.text.filter.rule;

import java.util.Collection;

import com.sysomos.text.sentenceiterator.TweetItem;

public class IgnoreIfIdsNotInRule extends TweetItemRule {

	private final Collection<Long> ids;

	public IgnoreIfIdsNotInRule(Collection<Long> ids) {
		this.ids = ids;
	}

	@Override
	public boolean fires(TweetItem item) {
		return item == null || item.user == null
				|| (ids != null && !ids.contains(item.user.id));
	}

}
