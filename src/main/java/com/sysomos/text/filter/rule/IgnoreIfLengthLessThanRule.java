package com.sysomos.text.filter.rule;

import org.apache.commons.lang3.StringUtils;

public class IgnoreIfLengthLessThanRule extends TextRule {

	private static final int MIN_LENGTH = 3;


	@Override
	public boolean fires(String text) {
		return !StringUtils.isEmpty(text) && text.length() < MIN_LENGTH;
	}

}
